﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calc.aspx.cs" Inherits="Calc.calc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calculadora - por Thiago Guimarães</title>
    <link href="assets/stylesheet/reset.css" rel="stylesheet" />
    <link href="assets/stylesheet/app.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h1 class="title">Calculadora</h1>
        <h2 class="subtitle">Thiago Guimarães</h2>

        <form id="calcForm" runat="server">
            <asp:BulletedList ID="History" CssClass="history" runat="server"></asp:BulletedList>
            <table class="calc">
                <tbody>
                    <tr>
                        <td colspan="4"><asp:TextBox ID="Result" runat="server" ReadOnly="true" CssClass="inputbox"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="CalcC" runat="server" CssClass="btn" Text="C" OnClick="CalcC_Click1" /></td>
                        <td>&nbsp;</td>
                        <td><asp:Button ID="RemoveLastNumber" runat="server" CssClass="btn" Text="<" OnClick="RemoveLastNumber_Click" /></td>
                        <td><asp:Button ID="ButtonDivider" runat="server" CssClass="btn" Text="/" OnClick="AddValueToField" /></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="ButtonOne" runat="server" CssClass="btn" Text="1" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonTwo" runat="server" CssClass="btn" Text="2" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonThree" runat="server" CssClass="btn" Text="3" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonMultiply" runat="server" CssClass="btn" Text="*" OnClick="AddValueToField" /></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="ButtonFour" runat="server" CssClass="btn" Text="4" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonFive" runat="server" CssClass="btn" Text="5" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonSix" runat="server" CssClass="btn" Text="6" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonMinus" runat="server" CssClass="btn" Text="-" OnClick="AddValueToField" /></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="ButtonSeven" runat="server" CssClass="btn" Text="7" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonEight" runat="server" CssClass="btn" Text="8" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonNine" runat="server" CssClass="btn" Text="9" OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonPlus" runat="server" CssClass="btn" Text="+" OnClick="AddValueToField" /></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="ButtonDot" runat="server" CssClass="btn" Text="." OnClick="AddValueToField" /></td>
                        <td><asp:Button ID="ButtonZero" runat="server" CssClass="btn" Text="0" OnClick="AddValueToField" /></td>
                        <td colspan="2"><asp:Button ID="ButtonEquals" runat="server" CssClass="btn" Text="=" OnClick="ButtonEquals_Click" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>

    <script src="assets/javascript/app.js"></script>
</body>
</html>
