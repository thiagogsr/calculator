﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calc
{
    public class Operation
    {
        private String expression;

        public Operation(String expression) {
            this.expression = expression;
        }

        public String calc() {
            char op = calc_operator();

            Decimal result = 0;
            if (op != ' ')
            {
                String[] operands = expression.Split(op);
                Decimal first_operand = Decimal.Parse(operands[0]);
                Decimal last_operand = Decimal.Parse(operands[1]);

                switch (op)
                {
                    case '+':
                        result = first_operand + last_operand;
                        break;
                    case '-':
                        result = first_operand - last_operand;
                        break;
                    case '*':
                        result = first_operand * last_operand;
                        break;
                    case '/':
                        result = first_operand / last_operand;
                        break;
                }
            }
            return result.ToString();
        }

        private char calc_operator() {
            char result = ' ';
            if (expression.IndexOf("+") > -1)
            {
                result = '+';
            }
            if (expression.IndexOf("-") > -1)
            {
                result = '-';
            }
            if (expression.IndexOf("*") > -1)
            {
                result = '*';
            }
            if (expression.IndexOf("/") > -1)
            {
                result = '/';
            }
            return result;
        }
    }
}