﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calc
{
    public partial class calc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddValueToField(object sender, EventArgs e)
        {
            String bt_text = ((Button) sender).Text;
            Result.Text += bt_text;
            CheckIfItIsAnOperantor(bt_text);
        }

        protected void CalcC_Click1(object sender, EventArgs e)
        {
            Result.Text = "";
            EnableOperators();
        }

        protected void ButtonEquals_Click(object sender, EventArgs e)
        {
            String current_text = Result.Text;
            String result = new Operation(current_text).calc();
            AddResultToHistory(current_text, result);
            Result.Text = result;
            EnableOperators();
        }

        protected void RemoveLastNumber_Click(object sender, EventArgs e)
        {
            String current_text = Result.Text;
            Result.Text = current_text.Substring(0, current_text.Length-1);
            CheckIfItIsAnOperantor(current_text);
        }

        private void AddResultToHistory(String expression, String result) {
            ListItem history_item = new ListItem(expression + " = " + result);
            History.Items.Add(history_item);
        }

        private void CheckIfItIsAnOperantor(String bt_text)
        {
            char[] operators = { '+', '-', '*', '/' };
            if (bt_text.IndexOfAny(operators) > -1)
            {
                DisableOperators();
            }
            else
            {
                EnableOperators();
            }
        }

        private void EnableOperators()
        {
            ButtonPlus.Enabled = true;
            ButtonMinus.Enabled = true;
            ButtonDivider.Enabled = true;
            ButtonMultiply.Enabled = true;
        }

        private void DisableOperators()
        {
            ButtonPlus.Enabled = false;
            ButtonMinus.Enabled = false;
            ButtonDivider.Enabled = false;
            ButtonMultiply.Enabled = false;
        }
    }
}